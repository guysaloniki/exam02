<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\Bonus;

/**
 * BonusSearch represents the model behind the search form about `app\models\Bonus`.
 */
 
     public $min_price = 100;
     public $max_price = 1000;
     
class BonusSearch extends Bonus
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'userId', 'amount', 'min_price', 'max_price'], 'integer'],
            [['reasonId'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Bonus::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'userId' => $this->userId,
          //  'amount' => $this->amount,
        ]);

        $query->andFilterWhere(['like', 'reasonId', $this->reasonId]);
         $query->andFilterWhere(['>=', 'min_price', $this->amount]);
         $query->andFilterWhere(['<=', 'max_price', $this->amount]);

        return $dataProvider;
    }
}
