<?php

namespace app\models;

use Yii;
use yii\db\ActiveRecord;
use yii\behaviors\BlameableBehavior;
use yii\helpers\ArrayHelper;

/**
 * This is the model class for table "bonusreason".
 *
 * @property integer $id
 * @property string $name
 */
class Bonusreason extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'bonusreason';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
		$rules = []; 
		$stringItems = [['notes'], 'string'];
		$integerItems  = ['status', 'created_at', 'updated_at', 'created_by', 'updated_by'];		
		if (\Yii::$app->user->can('updateLead')) {
			$integerItems[] = 'owner';
		}
		$integerRule = [];
		$integerRule[] = $integerItems;
		$integerRule[] = 'integer';
		$ShortStringItems = [['name', 'email', 'phone'], 'string', 'max' => 255]; 
		$rules[] = $stringItems;
		$rules[] = $integerRule;
		$rules[] = $ShortStringItems;		
		return $rules;
    }

     public static function getBonusreason()
    {
        $Pl= ArrayHelper::
                    map(self::find()->all(), 'id', 'name');
        return $Pl;                        
	}
   

	 public function behaviors()
    {
		return 
		[
			[
				'class' => BlameableBehavior::className(),
				'createdByAttribute' => 'created_by',
				'updatedByAttribute' => 'updated_by',
			 ],
				'timestamp' => [
				'class' => 'yii\behaviors\TimestampBehavior',
				'attributes' => [
					ActiveRecord::EVENT_BEFORE_INSERT => ['created_at', 'updated_at'],
					ActiveRecord::EVENT_BEFORE_UPDATE => ['updated_at'],
				],
			],
		];
    }


    /**
     * @inheritdoc
     */
     
	public function getUserOwner()
    {
        return $this->hasOne(User::className(), ['id' => 'owner']);
    }
	
	public function getCreatedBy()
    {
        return $this->hasOne(User::className(), ['id' => 'created_by']);
    }	

	public function getUpdateddBy()
    {
        return $this->hasOne(User::className(), ['id' => 'updated_by']);
    }	
    
    public function getStatusItem()
    {
        return $this->hasOne(Status::className(), ['id' => 'status']);
    }
	
	
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Name',
        ];
    }
}
