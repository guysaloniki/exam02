<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Bonusreason */

$this->title = 'Update Bonusreason: ' . $model->name;
$this->params['breadcrumbs'][] = ['label' => 'Bonusreasons', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->name, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="bonusreason-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
